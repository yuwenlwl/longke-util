<?php

namespace longke\longkeUtil;

class Lkutil
{
    public static function test($str)
    {
        return $str;
    }

    /**
     * 字符串转数组
     * @param $string
     * @param string $glue
     * @return array
     */
    public static function stringToArray($string, string $glue = ','): array
    {
        return explode($glue, $string);
    }
}